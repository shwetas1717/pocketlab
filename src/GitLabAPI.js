import AsyncStorage from '@react-native-community/async-storage';
import AppSettings from '@modules/AppSettings';

export default class GitLabAPI {
  constructor(returnSingleton = true) {
    if (returnSingleton) {
      if (GitLabAPI.instance) {
        return GitLabAPI.instance;
      }

      GitLabAPI.instance = this;
    }

    this.setConnection(undefined, undefined);

    return this;
  }

  get accessToken() {
    return this._accessToken;
  }

  setConnection(host, accessToken) {
    if (host === undefined) {
      host = 'https://gitlab.com';
    }
    this.url = `${host}/api/v4`;
    this._accessToken = accessToken;
  }

  hasConnection() {
    return this.accessToken !== undefined;
  }

  async todos() {
    return await this._doGETRequest('/todos');
  }

  async markTodoAsDone(id) {
    return await this._doPOSTRequest(`/todos/${id}/mark_as_done`);
  }

  async issue(project, issue) {
    return await this._doGETRequest(`/projects/${project}/issues/${issue}`);
  }

  async issueNotes(project, issue, sort = 'desc') {
    return await this._doGETRequest(
      `/projects/${project}/issues/${issue}/notes`,
      {sort, followPagination: true},
    );
  }

  async newIssue(project, body) {
    return await this._doPOSTRequest(`/projects/${project}/issues`, body);
  }

  async projects(options) {
    return await this._doGETRequest('/projects', {
      ...options,
      followPagination: true,
    });
  }

  async project(project) {
    return await this._doGETRequest(`/projects/${project}`);
  }

  async projectIssues(project, options) {
    return await this._doGETRequest(`/projects/${project}/issues/`, options);
  }

  async repository(project, options) {
    return await this._doGETRequest(`/projects/${project}/repository/tree`, {
      ...options,
      followPagination: true,
    });
  }

  async getBlob(project, sha) {
    return await this._doGETRequest(
      `/projects/${project}/repository/blobs/${sha}`,
    );
  }

  async searchProject(project, scope, query, ref = '') {
    const opts = {
      scope,
      search: query,
    };
    if (ref !== '') {
      opts.ref = ref;
    }
    return await this._doGETRequest(`/projects/${project}/search`, opts);
  }

  async pipelines(project, options = {}) {
    return await this._doGETRequest(`/projects/${project}/pipelines`, options);
  }

  async pipelineJobs(project, pipeline, options = {}) {
    return await this._doGETRequest(
      `/projects/${project}/pipelines/${pipeline}/jobs`,
      options,
    );
  }

  async jobLog(project, job, options = {}) {
    const response = await this._doRequest(
      `/projects/${project}/jobs/${job}/trace`,
      {...options, method: 'GET'},
    );
    if (response.status === 200) {
      return await response.text();
    } else {
      return false;
    }
  }

  async mergeRequest(project, merge_request) {
    return await this._doGETRequest(
      `/projects/${project}/merge_requests/${merge_request}`,
    );
  }

  async mergeRequestChanges(project, merge_request) {
    return await this._doGETRequest(
      `/projects/${project}/merge_requests/${merge_request}/changes`,
    );
  }

  async mergeRequestNotes(project, merge_request, sort = 'desc') {
    return await this._doGETRequest(
      `/projects/${project}/merge_requests/${merge_request}/notes`,
      {sort},
    );
  }

  async acceptMergeRequest(project, merge_request) {
    return await this._doPUTRequest(
      `/projects/${project}/merge_requests/${merge_request}/merge`,
    );
  }

  async user(id = null) {
    if (id === null) {
      return await this._doGETRequest('/user');
    } else {
      return await this._doGETRequest(`/users/${id}`);
    }
  }

  async mergeRequests(opts = {}) {
    return await this._doGETRequest('/merge_requests', opts);
  }

  async projectMergeRequests(project, options = {}) {
    return await this._doGETRequest(`/projects/${project}/merge_requests`, {
      ...options,
      followPagination: true,
    });
  }

  async starProject(project) {
    return await this._doPOSTRequest(`/projects/${project}/star`, {});
  }

  async _doPUTRequest(endpoint, body, opts = {}) {
    const response = await this._doRequest(endpoint, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    let result = await response.json();
    return result;
  }

  async _doPOSTRequest(endpoint, body, opts = {}) {
    const response = await this._doRequest(endpoint, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    console.log(response.status);
    if (response.status >= 200 && response.status < 300) {
      return await response.json();
    } else {
      return false;
    }
  }

  async _doGETRequest(endpoint, opts = {}) {
    try {
      const {followPagination} = opts;
      delete opts.followPagination;

      const search = Object.keys(opts)
        .map(key => `${key}=${opts[key]}`)
        .join('&');
      const finalEndpoint = endpoint + (search !== '' ? `?${search}` : '');
      const response = await this._doRequest(finalEndpoint, {method: 'GET'});
      const nextPage = response.headers.get('X-Next-Page');
      let result = await response.json();
      if (followPagination && (nextPage !== '' && nextPage != null)) {
        const nextResult = await this._doGETRequest(endpoint, {
          ...opts,
          page: nextPage,
          per_page: 20,
          followPagination,
        });
        result = result.concat(nextResult);
      }
      return result;
    } catch (e) {
      console.warn(e);
      return {error: true};
    }
  }

  async _doRequest(endpoint, opts = {}) {
    const {method} = opts;

    console.log(`Making ${method} request to ${this.url + endpoint}`);
    const headers = {
      ...opts.headers,
      Accept: 'application/json',
    };
    if (this.accessToken !== undefined) {
      headers['Private-Token'] = this.accessToken;
    }
    const response = await fetch(this.url + endpoint, {
      ...opts,
      headers,
    });
    return response;
  }
}
