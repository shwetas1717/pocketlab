import React, {useState} from 'react';
import {StyleSheet, Image, VirtualizedList, View, Text} from 'react-native';
import {
  Card,
  Paragraph,
  Headline,
  Caption,
  ActivityIndicator,
  Avatar,
} from 'react-native-paper';
import Markdown from 'react-native-markdown-renderer';
import MarkdownIt from 'markdown-it';
import moment from 'moment';
import HTML from 'react-native-render-html';
import {
  IGNORED_TAGS,
  alterNode,
  makeTableRenderer,
} from 'react-native-render-html-table-bridge';
import WebView from 'react-native-webview';

const md = MarkdownIt({
  typographer: true,
  html: true,
});

const config = {
  WebViewComponent: WebView,
};

const renderers = {
  table: makeTableRenderer(config),
};

const htmlConfig = {
  alterNode,
  renderers,
  ignoredTags: IGNORED_TAGS,
};

const rules = {
  html_block: (node, children, parents, style) => {
    console.log(node.content);
    return <HTML key={node.key} html={node.content} {...htmlConfig} />;
  },
};

const styles = StyleSheet.create({
  codeInline: {
    backgroundColor: '#fbe5e1',
    color: '#c0341d',
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 20,
    paddingRight: 2,
  },
});

const Discussion = props => {
  const {notes} = props;

  return (
    <VirtualizedList
      data={notes}
      getItemCount={() => notes.length}
      getItem={(data, index) => data[index]}
      keyExtractor={item => item.id}
      renderItem={({item}) => {
        const when = moment(item.created_at).fromNow();
        if (item.system) {
          return (
            <View style={{paddingLeft: 15}}>
              <Markdown rules={rules} markdownit={md}>
                {item.author.name} {item.body} {when}
              </Markdown>
            </View>
          );
        } else {
          return (
            <Card style={{margin: 10}}>
              <Card.Content>
                <View style={{flex: 1, flexDirection: 'row'}}>
                  <Avatar.Image
                    size={25}
                    source={{uri: item.author.avatar_url}}
                  />
                  <Text style={{flex: 1, marginLeft: 5, marginTop: 2}}>
                    {item.author.name}
                  </Text>
                  <Caption>{when}</Caption>
                </View>
                <Markdown rules={rules} markdownit={md}>
                  {item.body}
                </Markdown>
              </Card.Content>
            </Card>
          );
        }
      }}
      ListEmptyComponent={() => (
        <Paragraph style={{textAlign: 'center'}}>
          There are no comments
        </Paragraph>
      )}
    />
  );
};

export default Discussion;
