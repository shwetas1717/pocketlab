import React from 'react';
import {View, Text, Button} from 'react-native';
import {withRouter} from 'react-router-native';
import * as Sentry from '@sentry/react-native';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {hasError: false, error: null};
    this.previousError = null;
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return {hasError: true, error};
  }

  componentDidCatch(error, errorInfo) {
    if (
      this.previousError == null ||
      error.toString() !== this.prevError.toString()
    ) {
      this.prevError = error;
      Sentry.captureException(error);
    }
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <View>
          <Text style={{fontSize: 30, textAlign: 'center'}}>
            Something went wrong
          </Text>
          <Text style={{textAlign: 'center'}}>
            We've been made aware of the issue, and are working on a fix
          </Text>
          <Button
            title="Go Back"
            onPress={() => {
              this.setState({hasError: false, error: null});
              this.props.history.goBack();
            }}
          />
        </View>
      );
    }

    return this.props.children;
  }
}

export default withRouter(ErrorBoundary);
