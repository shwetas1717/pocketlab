import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, Linking} from 'react-native';
import {withTheme, Divider, List} from 'react-native-paper';
import {useHistory, useRouteMatch} from 'react-router-native';
import {withTitle} from '@components/Title';
import AppSettings from '@modules/AppSettings';
import URL from 'url-parse';
import packageFile from '../../../package.json';

const items = [
  {
    title: 'Account Settings',
    page: '/settings/accounts/',
    icon: 'account-multiple',
  },
];

const Index = props => {
  const [accounts, setAccounts] = useState([]);
  const history = useHistory();

  useEffect(() => {
    AppSettings.users().then(setAccounts);
  }, []);

  props.setTitle('Settings');
  return (
    <ScrollView>
      <List.Section>
        <List.Subheader>Accounts</List.Subheader>
        {accounts.map((item, index) => (
          <List.Item
            title={new URL(item.host).host}
            onPress={() => {
              history.push(`/accounts/edit/${index}`);
            }}
          />
        ))}
      </List.Section>
      <List.Section>
        <List.Subheader>About</List.Subheader>
        <List.Item title="Version" description={packageFile.version} />
        <List.Item
          title="Third-party Licenses"
          onPress={() => {
            history.push('/settings/licenses');
          }}
        />
        <List.Item
          title="View PocketLab on GitLab.com"
          onPress={() =>
            Linking.openURL('https://gitlab.com/alextiernan6/pocketlab')
          }
        />
      </List.Section>
    </ScrollView>
  );
};

export default withTitle(Index);
