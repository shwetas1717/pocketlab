import React, {useState, useEffect} from 'react';
import {StyleSheet, Image, ScrollView, View, FlatList} from 'react-native';
import {
  withTheme,
  List,
  Paragraph,
  Headline,
  Caption,
  ActivityIndicator,
  Avatar,
  FAB,
  Divider,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../../GitLabAPI';
import moment from 'moment';

const ListIssues = props => {
  const colors = props.theme.colors;
  const {params} = props.match;
  const [issues, setIssues] = useState(null);

  const styles = StyleSheet.create({
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
      backgroundColor: colors.primary,
      color: 'white',
    },
  });

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.projectIssues(params.project, {state: 'opened'}).then(response => {
      setIssues(response);
    });
  }, [params.project]);

  if (issues === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <>
      <FlatList
        data={issues}
        ItemSeparatorComponent={Divider}
        keyExtractor={item => item.id}
        renderItem={({item}) => {
          const when = moment(item.created_at).fromNow();
          return (
            <List.Item
              title={`${item.title}`}
              description={`#${item.iid} · ${when}`}
              onPress={() =>
                props.history.push(
                  `/projects/${params.project}/issues/${item.iid}`,
                )
              }
            />
          );
        }}
        ListEmptyComponent={() => (
          <Paragraph style={{textAlign: 'center'}}>
            There are no issues
          </Paragraph>
        )}
      />
      <FAB
        style={styles.fab}
        icon="plus"
        onPress={() =>
          props.history.push(`/projects/${params.project}/new-issue`)
        }
      />
    </>
  );
};

export default withRouter(withTheme(ListIssues));
