import React, {useState, useEffect} from 'react';
import {ActivityIndicator} from 'react-native-paper';
import GitLabAPI from '../../../GitLabAPI';
import URLSearchParams from '@ungap/url-search-params';
import ExtToComponent from './ExtToComponent.js';

const ViewBlob = props => {
  const {params} = props.match;
  const [content, setContent] = useState(null);
  const searchParams = new URLSearchParams(props.location.search);
  const ext = searchParams.get('ext');

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.getBlob(params.project, params.sha).then(response => {
      if (response.content) {
        setContent(response.content);
      }
    });
  }, [params.project, params.sha]);

  if (content === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return <ExtToComponent data={content} extension={ext} />;
  //return <SyntaxHighlighter style={atomOneLight}>{content}</SyntaxHighlighter>;
};

export default ViewBlob;
